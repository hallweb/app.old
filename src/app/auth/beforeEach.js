import store from './vuex'

const isAuthRoute = route => route.path.indexOf('/auth') !== -1
const isLogged = () => store.getters.isLogged(store.state)

export function beforeEach (to, from, next) {
  if (!isAuthRoute(to) && !isLogged()) {
    next('/auth')
  } else {
    next()
  }
}

export function auth (to, from, next) {
  if (isLogged()) {
    next('/blog')
  } else {
    next()
  }
}
