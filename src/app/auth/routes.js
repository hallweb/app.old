import Main from './components/Main'
import Login from './components/Forms/Login'
import Register from './components/Forms/Register'
import { auth as notAuth } from './beforeEach'

export default [
  {
    path: '/auth',
    component: Main,
    beforeEnter: notAuth,
    children: [
      {
        name: 'auth',
        path: '',
        component: Login
      },
      {
        name: 'register',
        path: 'register',
        component: Register
      }
    ]
  }
]
