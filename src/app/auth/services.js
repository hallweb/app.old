import http from '@/http'

export const postLogin = (email, password) => {
  return http.post('api/login', {
    username: email,
    password: password
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response.data
    })
}

export const getUser = () => {
  http.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token')
  return http.get('user')
    .then(response => response.data)
    .catch(error => {
      throw error.response.data.error
    })
}
