import * as types from './mutations-types'
import { postLogin, getUser } from '../services'
import router from '@/router'

export const attemptLogin = ({ commit, dispatch }, payload) => {
  return postLogin(payload.email, payload.password)
    .then(data => {
      // context.commit(types.setUser, data)
      const now = new Date()
      const expirationDate = new Date(now.getTime() + data.expires_in * 1000)
      localStorage.setItem('token', data.access_token)
      localStorage.setItem('expirationDate', expirationDate)
      commit(types.setToken, data.access_token)
      dispatch('userAuth')
      // dispatch('setLogoutTimer', data.expires_in)
      // router.replace('/forum')
      // router.go({
      //   path: '/forum',
      //   force: true
      // })
    }, err => {
      // dispatch('removeToken')
      throw err
    })
}
export const userAuth = ({ commit, dispatch }) => {
  return getUser()
    .then(response => {
      commit(types.setUser, response)
    }, err => {
      // dispatch('removeToken')
      throw err
    })
}

export const tryAutoLogin = ({ commit, dispatch }) => {
  const token = localStorage.getItem('token')
  if (!token) {
    return
  }
  const expirationDate = localStorage.getItem('expirationDate')
  const now = new Date()
  // if (now >= expirationDate) {
  //   dispatch('removeToken')
  //   return
  // }
  commit(types.setToken, token)
  dispatch('userAuth')
}

export const setLogoutTimer = ({ dispatch }, expirationTime) => {
  setTimeout(() => {
    dispatch('logout')
  }, expirationTime * 1000)
}

export const removeToken = () => {
  localStorage.removeItem('token')
  localStorage.removeItem('expirationDate')
}

export const logout = ({ commit, dispatch }) => {
  dispatch('removeToken')
  commit(types.clearData)
  router.go({path: '/blog', force: true})
}
