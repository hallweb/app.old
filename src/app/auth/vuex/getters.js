import { isEmpty } from 'lodash'

export const isLogged = ({ token }) => !isEmpty(localStorage.getItem('token'))
export const currentUser = ({ user }) => user
