export const setToken = 'auth/setToken'
export const setUser = 'auth/setUser'
export const clearData = 'auth/clearData'
