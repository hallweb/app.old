import * as types from './mutations-types'

export default {
  [types.setUser] (state, user) {
    state.user = user
  },
  [types.setToken] (state, token) {
    state.token = token
  },
  [types.clearData] (state) {
    state.user = null
    state.token = null
  }
}
