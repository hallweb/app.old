import Main from './components/Main'
import Article from './components/Content/Article'
import BlogFeed from './components/Content/BlogFeed'
import ArticleList from './components/Content/ArticleList'
import Contact from './components/Form/Contact'

export default [
  {
    path: '/blog',
    component: Main,
    children: [
      {
        name: 'blog',
        path: '/', //router-view
        component: BlogFeed
      },
      {
        name: 'author-article-list',
        path: 'list',
        component: ArticleList
      },
      {
        name: 'contact',
        path: 'contact',
        component: Contact
      },
      {
        name: 'blog-post',
        path: ':id',
        component: Article
      }
    ]
  }
]
