import Main from './components/Main'
import AddThread from './components/Content/AddThread'
import EditThread from './components/Content/EditThread'
import Threads from './components/Content/Threads'
import Forum from './components/Content/Forum'
import Thread from './components/Content/Thread'
import {beforeEach as auth} from '@/app/auth/beforeEach'

export default [
  {
    path: '/forum',
    component: Main,
    beforeEnter: auth,
    children: [
      {
        name: 'forum',
        path: '',
        component: Forum
      },
      {
        name: 'forum-threads',
        path: 'sub/:id',
        component: Threads
      },
      {
        name: 'forum-thread',
        path: 'thread/:id',
        component: Thread
      },
      {
        name: 'forum-addthread',
        path: 'thread/:id/add',
        component: AddThread
      },
      {
        name: 'forum-editthread',
        path: 'thread/:id/edit',
        component: EditThread
      }
    ]
  }
]
