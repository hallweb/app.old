import { routes as auth } from './auth'
import { routes as user } from './user'
import { routes as forum } from './forum'
import { routes as blog } from './blog'

export default [ ...auth, ...user, ...forum, ...blog ]
