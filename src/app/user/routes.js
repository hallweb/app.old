import Main from './components/Main'
import Users from './components/Content/Users'
import Profile from './components/Content/Profile'
import Threads from './components/Content/Threads'
import { beforeEach as auth } from '@/app/auth/beforeEach'

export default [
  {
    path: '/user',
    component: Main,
    beforeEnter: auth,
    children: [
      {
        name: 'civis',
        path: 'civis',
        component: Users
      },
      {
        name: 'profile',
        path: 'profile',
        component: Profile
      },
      {
        name: 'my-threads',
        path: 'threads',
        component: Threads
      }
    ]
  }
]
