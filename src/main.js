import Vue from 'vue'
import App from './app/Main.vue'
import router from './router'
import store from './vuex'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate'
import Croppa from 'vue-croppa'
import VueSweetAlert from 'vue-sweetalert2'

Vue.use(BootstrapVue, store)
Vue.use(Croppa)
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields'
})
Vue.use(VueSweetAlert)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  beforeCreate () {
    this.$store.dispatch('tryAutoLogin')
  },
  render: h => h(App)
})
