import loaderFactory from './loaderFactory'
import registerStore from './registerStore'
import PageLoader from './components/page-loader'

const install = (Vue, store) => {
  Vue.component('PageLoader', PageLoader)
  registerStore(store)
  Object.defineProperty(Vue.prototype, '$loader', {
    get () {
      return loaderFactory(this)
    }
  })
  // show () {
  //   console.log({show: this})
  // },
  // hide () {
  //   console.log({hide: this})
  // }
}
export default { install }
