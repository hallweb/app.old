import module from './vuex'

const registerStore = store => {
  store.registerModule('HALL_LOADER', { ...module })
}

export default registerStore
