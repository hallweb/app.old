import Pusher from 'pusher-js'

Pusher.logToConsole = true
/**
 * aqui inicia a configuração do pusher
 * new Pusher(APP_KEY, {})
 */
const socket = new Pusher('bc1b47eaba2b4e2576e9', {
  cluster: 'us2',
  encrypted: true
})

export default socket
/**
 * função para executar o pusher, para ser reutilizado
 * @param string channel
 * @param string event
 * @param function payload
 * @param this context
 */
// export const getPusher = (channel, event, payload, context) => {
//   socket.subscribe(channel)
//   return socket.bind(event, payload, context)
// }
