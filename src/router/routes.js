import { routes as app } from '../app'

const root = [
  {path: '/', redirect: '/blog'}
]

export default [ ...root, ...app ]
