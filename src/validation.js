import { Validator } from 'vee-validate'

Validator.dictionary.merge({
  en: {
    messages: {
      confirmed: function () {
        return 'As senhas devem ser iguais!'
      },
      required: function (fieldName) {
        return `Por favor o preencha ${fieldName} corretamente!`
      },
      alpha_spaces: function () {
        return 'Seu nome deve conter apenas letras!'
      },
      alpha_dash: function () {
        return 'Seu username não pode conter espaços, somente letras e números!'
      },
      email: function () {
        return 'Digite um email válido!'
      },
      min: function (fieldName, params) {
        return `Campo deve conter no minimo ${params} caracteres!`
      }
    }
  }
})
